package org.hscoder.springboot.hbase;

public class DeviceState {
    private String id;
    private String name;
    private String state;

    public DeviceState() {
    }

    public DeviceState(String id, String name, String state) {
        this.id = id;
        this.name = name;
        this.state = state;
    }

    public DeviceState(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
