package org.hscoder.springboot.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

@SpringBootApplication
public class WebBoot {

    public static void main(String[] args) throws Exception {
        SpringApplication app = new SpringApplication(WebBoot.class);

        // 指定PID生成，默认输出到application.pid
        app.addListeners(new ApplicationPidFileWriter());
        app.run(args);
    }

    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        return new MethodValidationPostProcessor();
    }
}
