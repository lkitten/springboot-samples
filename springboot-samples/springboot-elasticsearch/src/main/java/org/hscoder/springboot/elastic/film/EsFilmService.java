package org.hscoder.springboot.elastic.film;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EsFilmService {

    private static final Logger logger = LoggerFactory.getLogger(EsFilmService.class);

    @Autowired
    private EsFilmRepository filmRepository;

    public List<EsFilm> listByPeriod(String period){

        return filmRepository.findByPeriod(period, PageRequest.of(1, 20)).getContent();
    }
}
