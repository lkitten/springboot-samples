package org.hscoder.springboot.elastic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElasticBoot {

    public static void main(String[] args) {
        SpringApplication.run(ElasticBoot.class);
    }


//    //Embedded Elasticsearch Server
//    @Bean
//    public ElasticsearchOperations elasticsearchTemplate() {
//        return new ElasticsearchTemplate(new TransportClient().local(true).node().client());
//    }
}


