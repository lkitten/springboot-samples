package org.hscoder.springboot.elastic.repository;

import org.hscoder.springboot.elastic.domain.EsBook;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface EsBookRepository extends ElasticsearchRepository<EsBook, String>, EsBookRepositoryCustom {

    Page<EsBook> findByType(String type, Pageable pageable);

    List<EsBook> findByTagsIn(List<String> tags);
}
