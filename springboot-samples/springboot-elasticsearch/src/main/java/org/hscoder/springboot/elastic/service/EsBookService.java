package org.hscoder.springboot.elastic.service;

import org.hscoder.springboot.elastic.domain.EsBook;
import org.hscoder.springboot.elastic.repository.EsBookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class EsBookService {

    @Autowired
    private EsBookRepository bookRepository;

    /**
     * 创建书籍信息
     *
     * @param author
     * @param type
     * @param title
     * @param publishDate
     * @return
     */
    public EsBook createEsBook(String author, String type, String title, List<String> tags, Date publishDate) {

        if (StringUtils.isEmpty(type) || StringUtils.isEmpty(title) || author == null || publishDate == null) {
            return null;
        }

        EsBook book = new EsBook();
        book.setType(type);
        book.setTitle(title);

        book.setTags(tags);
        book.setAuthor(author);
        book.setPublishDate(publishDate);
        return bookRepository.save(book);
    }


    /**
     * 更新书籍信息
     *
     * @param bookId
     * @param type
     * @param title
     * @return
     */
    public boolean updateEsBook(String bookId, String type, String title) {
        if (StringUtils.isEmpty(bookId) || StringUtils.isEmpty(title)) {
            return false;
        }

        EsBook book = bookRepository.findById(bookId).orElse(null);
        if (book == null) {
            return false;
        }

        book.setType(type);
        book.setTitle(title);
        return bookRepository.save(book) != null;
    }

    /**
     * 更新书籍 标签信息
     *
     * @param bookId
     * @param tags
     * @return
     */
    public boolean updateTags(String bookId, List<String> tags) {
        if (StringUtils.isEmpty(bookId)) {
            return false;
        }

        EsBook book = bookRepository.findById(bookId).orElse(null);
        if (book == null) {
            return false;
        }

        book.setTags(tags);
        return bookRepository.save(book) != null;
    }

    /**
     * 删除书籍信息
     *
     * @param bookId
     * @return
     */
    public boolean deleteEsBook(String bookId) {
        if (StringUtils.isEmpty(bookId)) {
            return false;
        }

        Optional<EsBook> book = bookRepository.findById(bookId);
        if (book.isPresent()) {
            bookRepository.delete(book.get());
            return true;
        }
        return false;
    }

    /**
     * 根据编号查询
     *
     * @param bookId
     * @return
     */
    public EsBook getEsBook(String bookId) {
        if (StringUtils.isEmpty(bookId)) {
            return null;
        }

        return bookRepository.findById(bookId).orElse(null);
    }


    /**
     * 根据标签检索
     * @param tags
     * @return
     */
    public List<EsBook> listByTag(List<String> tags) {

        return bookRepository.findByTagsIn(tags);
    }

    /**
     * 获取分类的前10条，按出版时间排序
     *
     * @param type
     * @return
     */
    public List<EsBook> listTop10ByType(String type) {

        PageRequest pageRequest = PageRequest.of(0, 10, Sort.by(Sort.Direction.DESC, "publishDate"));
        Page<EsBook> pageResult = bookRepository.findByType(type, pageRequest);

        return pageResult.getContent();
    }
}
